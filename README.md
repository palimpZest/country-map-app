# **World Map App**

#### **[![Click to see a live version]()](http://eager-easley-293649.netlify.com)**

##### **How to use**

1. Clone the project.
2. Go to folder
```
yarn install && yarn start
```
or
```
npm install && npm start
```