import React, {Component} from "react";
import { compose, withProps } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";

const MyMapComponent = compose(
  withProps({
    googleMapURL:
      "https://maps.googleapis.com/maps/api/js?key=AIzaSyAyesbQMyKVVbBgKVi2g6VX7mop2z96jBo&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `calc(100vh - 60px)` }} />,
    containerElement: <div style={{ height: `calc(100vh - 60px)` }} />,
    mapElement: <div style={{ height: `calc(100vh - 60px)` }} />
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap defaultZoom={2.2} defaultCenter={{ lat: 35, lng: 0 }}>
    {props.isMarkerShown && (
      <div>
        {Object.keys(props.markers).map((marker, index) => (
          <Marker
            position={{
              lat: parseInt(
                Object.values(props.markers[index]["latlng"])[0],
                10
              ),
              lng: parseInt(
                Object.values(props.markers[index]["latlng"])[1],
                10
              )
            }}
            key={index}
            onClick={props.onMarkerClick}
          />
        ))}
      </div>
    )}
  </GoogleMap>
));

class WorldMap extends Component {
  constructor(props) {
    super(props);
    this.state = { isMarkerShown: false };
  }

  componentDidMount() {
    this.delayedShowMarker()
  }

  delayedShowMarker = () => {
    setTimeout(() => {
      this.setState({ isMarkerShown: true })
    }, 3000)
  }

  handleMarkerClick = () => {
    this.setState({ isMarkerShown: false })
    this.delayedShowMarker()
  }

  render(props) {
    return <div className="GoogleMap">
        <MyMapComponent 
          isMarkerShown={this.state.isMarkerShown} 
          onMarkerClick={this.handleMarkerClick}  
          markers={this.props.markers}
        />
      </div>;
  }
}

export default WorldMap;
