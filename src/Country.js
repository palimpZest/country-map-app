import React, {Component} from 'react';

class Country extends Component {
  render() {
    return <button 
            onClick={this.props.handleSelectedCountry.bind(this, this.props.name)} 
            className="Country"
            >
        <h3>{this.props.name}</h3>
        <div className="CountryDataHolder">
          <div>
            <span className="CardItemField">Population</span>
            <br />
            <br />
            <span className="CardItemData">{this.props.population}</span>
            <br />
            <br />
          </div>
          <div>
            <span className="CardItemField">Capital</span>
            <br />
            <br />
            <span className="CardItemData">{this.props.capital}</span>
            <br />
            <br />
          </div>
          <div>
            <span className="CardItemField">Borders</span>
            <br />
            <br />
            <span className="CardItemData">
              <div className="BorderDataHolder">
                {this.props.borders.length === 0
                  ? " No Borders"
                  : this.props.borders.map((item, index) => (
                      <li key={index} className="border-text">{item}</li>
                    ))
                }
              </div>
            </span>
          </div>
        </div>
      </button>;
  }
}

export default Country;
