import React, {Component} from "react";
import CountriesHolder from "./CountriesHolder";
import WorldMap from "./WorldMap";

class ControlDisplayBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      populationSort: null,
      alphabetSort: false
    };
    this.handlePopulationSort = this.handlePopulationSort.bind(this);
    this.handleAlphabetSort = this.handleAlphabetSort.bind(this);
    this.handleSelectedCountry = this.handleSelectedCountry.bind(this);
  }

  updateSearch(event) {
    this.setState({ search: event.target.value });
  }

  handlePopulationSort() {
    this.setState({ populationSort: !this.state.populationSort });
  }

  handleAlphabetSort(name) {
    this.setState({
      alphabetSort: !this.state.alphabetSort,
      populationSort: null
    });
  }

  handleSelectedCountry(name) {
    const countrySelected = name.join("");
    this.setState({ search: countrySelected });
  }

  handleReset() {
    this.setState({
      search: "",
      populationSort: null,
      alphabetSort: false
    });
  }

  render() {
    let popArray = [];
    const alphabetState = this.state.alphabetSort;
    const populationState = this.state.populationSort;
    const countries = this.props.items
      .filter(country => {
        return country.name
            .toLowerCase()
            .indexOf(this.state.search.toLowerCase()) !== -1;
      })
      .sort(function(a, b) {
        const nameA = a.name.toLowerCase();
        const nameB = b.name.toLowerCase();
        if (alphabetState === false) {
          if (nameA < nameB) {
            return -1;
          }
          if (nameA < nameB) {
            return 1;
          }
        } else if (alphabetState === true) {
          if (nameA < nameB) {
            return 1;
          }
          if (nameA < nameB) {
            return -1;
          }
        }
        return 0;
      })
      .sort(function(a, b) {
        if (populationState === false) {
          return b.population - a.population;
        } else if (populationState === true) {
          return a.population - b.population;
        } else {
          return alphabetState === null;
        }
      });
    return (
      <div>
        <div className="ControlBar">
          <h3 className="App-title">World Map App</h3>
          <input
            className="SearchInput"
            type="text"
            value={this.state.search}
            onChange={this.updateSearch.bind(this)}
            placeholder="Search country"
          />
          <button
            className="resetButton"
            onClick={this.handleReset.bind(this)}
          >
          Reset
          </button>
          <button
            className="SortByName"
            onClick={this.handleAlphabetSort.bind(this)}
          >
            Sort by Name
          </button>
          <button
            className="SortByPopulation"
            onClick={this.handlePopulationSort.bind(this)}
          >
            Sort by Population
          </button>
          <div className="PopulationData">
            <div>
              {countries.length > 1 
                ? `Average population of ${countries.length} selected countries` 
                : `Population of selected country`  
              }
            </div>
            <div>
              {Object.values(countries).map((population, index) => {
                const populations = countries[index]["population"];
                popArray.push(populations);
                return null;
              })}
            </div>
            <div className="averageNumber">
              {Math.ceil(Object.values(popArray).reduce((a, b, _, arr) => a + b / arr.length, 0))}
            </div>
          </div>
        </div>
        <div className="Blocks">
          <div className="ControlDisplayBar">
            {Object.keys(countries).map((keyName, keyIndex) => {
              const name = Object.values(countries[keyIndex]["name"]);
              const population = countries[keyIndex]["population"];
              const capital = countries[keyIndex]["capital"];
              const borders = Object.values(countries[keyIndex]["borders"]);
              return (
                <CountriesHolder
                  key={keyIndex}
                  name={name}
                  capital={capital}
                  population={population}
                  borders={borders}
                  handleSelectedCountry={this.handleSelectedCountry.bind(this)}
                  className="CountriesHolder"
                />
              );
            })}
          </div>
          <WorldMap markers={countries} />
        </div>
      </div>
    );
  }
}

export default ControlDisplayBar;
