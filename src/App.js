import React, { Component } from 'react';
import './App.css';
import axios from "axios";
import ControlDisplayBar from "./ControlDisplayBar";

const URL_API = "https://restcountries-v1.p.mashape.com/";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
  }

  componentDidMount() {
    axios(URL_API, {
      method: "get",
      headers: {
        "X-Mashape-Key": "l5eMXwY6d3mshmvnljsx6GVH9YWxp1IsKhsjsnSAZ5yXpYiGRl",
        "Content-Type": "application/x-www-form-urlencoded"
      }
    })
      .then(response => {
        this.setState({
          items: response.data
        });
      })
      .catch(error => console.log(error));
  }

  render() {
    return <div className="App">
        <ControlDisplayBar items={this.state.items} />
      </div>;
  }
}

export default App;
