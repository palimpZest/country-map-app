import React, { Component } from "react";
import "./App.css";
import Country from "./Country";

class CountriesHolder extends Component {
  render() {
    const { name, 
            capital, 
            population, 
            borders, 
            handleSelectedCountry 
          } = this.props;

    return <Country   
              name={name} 
              capital={capital} 
              population={population} 
              borders={borders} 
              handleSelectedCountry={handleSelectedCountry}
            >
      </Country>;
  }
}

export default CountriesHolder;
